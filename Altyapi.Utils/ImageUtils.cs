﻿using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;
using Omu.Drawing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Altyapi.Utils
{
	public static class ImageUtils
	{
		public static void SaveJpeg(string path, Image img, int quality)
		{
			if (quality < 0 || quality > 100)
				throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");

			// Encoder parameter for image quality 
			EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, quality);
			// JPEG image codec 
			ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
			EncoderParameters encoderParams = new EncoderParameters(1);
			encoderParams.Param[0] = qualityParam;
			img.Save(path, jpegCodec, encoderParams);
		}

		/// <summary> 
		/// Returns the image codec with the given mime type 
		/// </summary> 
		private static ImageCodecInfo GetEncoderInfo(string mimeType)
		{
			// Get image codecs for all image formats 
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

			// Find the correct image codec 
			for (int i = 0; i < codecs.Length; i++)
				if (codecs[i].MimeType == mimeType)
					return codecs[i];

			return null;
		}

		#region ResimFiziksel Sil
		public static void ResimFizikselSil(Resim resim)
		{
			var buyukDosyaYolu = resim.ResimUrlBuyuk;
			var kucukDosyaYolu = resim.ResimUrlKucuk;
			var buyukTamYol = HttpContext.Current.Server.MapPath(buyukDosyaYolu);
			var kucukTamYol = HttpContext.Current.Server.MapPath(kucukDosyaYolu);

			var buyukResim = new FileInfo(buyukTamYol);
			var kucukResim = new FileInfo(kucukTamYol);

			if (buyukResim.Exists)
			{
				buyukResim.Delete();
			}

			if (kucukResim.Exists)
			{
				kucukResim.Delete();
			}
		}
		#endregion

		#region Resimleri Boyutla&Kaydet
		public static List<string> ResimBoyutlaKaydet(HttpPostedFileBase yüklenenResim,string anaDizin)
		{
			var resimOrj = Image.FromStream(yüklenenResim.InputStream, true, true);
			var resimKucuk = Imager.Resize(resimOrj, 200, 150, true);
			var resimBuyuk = Imager.Resize(resimOrj, 1200, 1000, true);
			var dosyaAdi = Guid.NewGuid().ToString().Replace("-", "");
			var uzanti = Path.GetExtension(yüklenenResim.FileName);
			var buyukTamYol = anaDizin + dosyaAdi + uzanti;
			var kucukTamYol = anaDizin + "thumb_" + dosyaAdi + uzanti;

			if (!Directory.Exists(HttpContext.Current.Server.MapPath(anaDizin)))
			{
				Directory.CreateDirectory(HttpContext.Current.Server.MapPath(anaDizin));
			}

			ImageUtils.SaveJpeg(HttpContext.Current.Server.MapPath(kucukTamYol), resimKucuk, 90);
			ImageUtils.SaveJpeg(HttpContext.Current.Server.MapPath(buyukTamYol), resimBuyuk, 90);

			return new List<string>() { kucukTamYol, buyukTamYol };
		}
		#endregion


		#region Galeri Boyutla&Kaydet
		public static List<Resim> GaleriBoyutlaKaydet(List<HttpPostedFileBase> galeri)
		{
			var resimler = new List<Resim>();
			foreach (var item in galeri)
			{
				if (item == null) continue;
				var resimOrj = Image.FromStream(item.InputStream, true, true);
				var resimKucuk = Imager.Resize(resimOrj, 200, 150, true);
				var resimBuyuk = Imager.Resize(resimOrj, 1200, 1000, true);
				var dosyaAdi = Guid.NewGuid().ToString().Replace("-", "");
				var uzanti = Path.GetExtension(item.FileName);
				var buyukTamYol = "/External/Galeri/" + dosyaAdi + uzanti;
				var kucukTamYol = "/External/Galeri/" + "thumb_" + dosyaAdi + uzanti;

				ImageUtils.SaveJpeg(HttpContext.Current.Server.MapPath(kucukTamYol), resimKucuk, 90);
				ImageUtils.SaveJpeg(HttpContext.Current.Server.MapPath(buyukTamYol), resimBuyuk, 90);

				resimler.Add(new Resim { ResimUrlBuyuk = buyukTamYol, ResimUrlKucuk = kucukTamYol, Siralama = 1 });
			}
			return resimler;
		}
		#endregion
	}
}
