﻿using System.Linq;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.Model;
using Crypto = System.Web.Helpers.Crypto;


namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    public class AccountController : Controller
    {
        private readonly IKullaniciRepository _kullaniciRepository;

        public AccountController(IKullaniciRepository kullaniciRepository)
        {
            _kullaniciRepository = kullaniciRepository;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Kullanici kullanici)
        {

            var hashpass = Crypto.Hash(kullanici.Sifre);
            var kullaniciVarMi = _kullaniciRepository.GetMany(x => x.KullaniciAdi == kullanici.KullaniciAdi && x.Sifre.Equals(hashpass) && x.AktifMi == true).SingleOrDefault();

            if (kullaniciVarMi != null) {

                if (kullaniciVarMi.Roller.Any(q => q.Ad=="Admin")) {

                    Session["KullaniciAdi"] = kullaniciVarMi.KullaniciAdi;
                    Session["Kullanici"] = kullaniciVarMi;
                    return RedirectToAction("Index","Home");
                }
                ViewBag.Mesaj = "Yetkisiz Kullanıcı";
                return View();
            }
            ViewBag.Mesaj = "Kullanıcı Adı ve Şifre Hatalı";
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _kullaniciRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}