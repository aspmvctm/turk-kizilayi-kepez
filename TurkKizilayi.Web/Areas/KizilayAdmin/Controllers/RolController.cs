﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data;
using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;
using TurkKizilayi.Web.CustomFilter;

namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    [LoginFilter]
    public class RolController : Controller
    {
        #region Constructor
        private readonly IRolRepository _rolRepository;
        private readonly IKullaniciRepository _kullaniciRepository;

        public RolController(IRolRepository rolRepository, IKullaniciRepository kullaniciRepository)
        {
            _rolRepository = rolRepository;
            _kullaniciRepository = kullaniciRepository;
        }
        #endregion

        [HttpGet]
        public ActionResult Listele()
        {
            var viewModelRol = from rol in _rolRepository.GetAll().AsEnumerable()
                                    orderby rol.ID descending
                                    select new ViewModelRol
                                    {
                                        ID = rol.ID,
                                        Ad = rol.Ad,
                                        Durumu = rol.AktifMi
                                    };
            return View(viewModelRol);
        }

        [HttpGet]
        public ActionResult Ekle()
        {
            return View(new ViewModelRol());
        }

        [HttpPost]
        public JsonResult Ekle(ViewModelRol viewModelRol)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _rolRepository.Insert(PreEkleGuncelle(viewModelRol));
                    _rolRepository.Save();

                    return Json(
                        new ResultJson
                        {
                            Message = "Rol ekleme işlemi başarılı.",
                            MType = "success",
                            Title = "Rol Eklendi",
                            RedirectURL = "/KizilayAdmin/Rol/Listele"
                        });
                }
                else
                {
                    return Json(new ResultJson { Message = "Lütfen zorunlu alanları doldurunuz.", MType = "error", Title = "Hata" });
                }
            }
            catch (Exception ex)
            {
                return Json(new ResultJson { Message = "Bir Hata Oluştu :" + ex, MType = "error", Title = "Hata" });
            }
        }

        [HttpGet]
        public ActionResult Guncelle(int ID)
        {
            var rol = _rolRepository.GetById(ID);
            if (rol == null)
            {
                throw new Exception("Rol kayıt bulunamadı.");
            }
            return View(new ViewModelRol() { ID = rol.ID, Ad = rol.Ad, Durumu = rol.AktifMi});
        }

        [HttpPost]
        public JsonResult Guncelle(ViewModelRol viewModelRol)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _rolRepository.Update(PreEkleGuncelle(viewModelRol));
                    _rolRepository.Save();

                    return Json(
                      new ResultJson
                      {
                          Message = "Rol güncelleme işlemi başarılı.",
                          MType = "success",
                          Title = "Rol Güncellendi",
                          RedirectURL = "/KizilayAdmin/Rol/Listele"
                      });
                }
                else
                {
                    return Json(new ResultJson { Message = "Lütfen zorunlu alanları doldurunuz.", MType = "error", Title = "Hata" });
                }
            }
            catch (Exception ex)
            {
                return Json(new ResultJson { Message = "Bir Hata Oluştu :" + ex, MType = "error", Title = "Hata" });
            }
        }

        [HttpPost]
        public ActionResult Sil(int ID)
        {
            var rol = _rolRepository.GetById(ID);
            if (rol == null)
            {
                return Json(new ResultJson { Message = "Rol silmede kayıt bulunamadı", MType = "error", Title = "Hata" });
            }
            _rolRepository.Delete(rol);
            _rolRepository.Save();

            return Json(new ResultJson { Message = "Rol silme işlemi başarılı.", MType = "success", Title = "Rol Silindi" });
        }

        #region PreEkleGuncelle Rol
        public Rol PreEkleGuncelle(ViewModelRol viewModelRol)
        {
            var rol = new Rol();
            var sessionID = Session["KullaniciAdi"].ToString();
            var kullaniciID = _kullaniciRepository.Get(x => x.KullaniciAdi == sessionID && x.AktifMi == true).ID;
            if (viewModelRol.ID == 0)
            {
                rol.EklenmeTarihi = DateTime.Now;
            }
            else
            {
                rol = _rolRepository.GetById(viewModelRol.ID);
                rol.DegistirmeTarihi = DateTime.Now;
                rol.GuncelleyenKullaniciId = kullaniciID;
                rol.Versiyon = rol.Versiyon + 1;
            }

            rol.Ad = viewModelRol.Ad;
            rol.AktifMi = viewModelRol.Durumu;
            rol.EkleyenKullaniciId = kullaniciID;

            return rol;
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            _rolRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}