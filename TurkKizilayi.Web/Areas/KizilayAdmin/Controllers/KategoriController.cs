﻿using System;
using System.Linq;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data;
using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;
using TurkKizilayi.Web.CustomFilter;

namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    [LoginFilter]
    public class KategoriController : Controller
    {
        #region Constructor
        private readonly IKategoriRepository _kategoriRepository;
        private readonly IKullaniciRepository _kullaniciRepository;

        public KategoriController(IKategoriRepository kategoriRepository, IKullaniciRepository kullaniciRepository)
        {
            _kategoriRepository = kategoriRepository;
            _kullaniciRepository = kullaniciRepository;
        }
        #endregion

        [HttpGet]
        public ActionResult Listele()
        {
            var viewModelKategori = from kategori in _kategoriRepository.GetAll().AsEnumerable()
                                    orderby kategori.ID descending
                                    select new ViewModelKategori
                                    {
                                        ID = kategori.ID,
                                        Ad = kategori.Ad,
                                        Durumu = kategori.AktifMi,
                                        Siralama = kategori.Siralama
                                    };
            return View(viewModelKategori);
        }

        [HttpGet]
        public ActionResult Ekle()
        {
            return View(new ViewModelKategori());
        }

        [HttpPost]
        public JsonResult Ekle(ViewModelKategori viewModelKategori)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _kategoriRepository.Insert(PreEkleGuncelle(viewModelKategori));
                    _kategoriRepository.Save();

                    return Json(
                        new ResultJson
                        {
                            Message = "Kategori ekleme işlemi başarılı.",
                            MType = "success",
                            Title = "Kategori Eklendi",
                            RedirectURL = "/KizilayAdmin/Kategori/Listele"
                        });
                }
                else
                {
                    return Json(new ResultJson { Message = "Lütfen zorunlu alanları doldurunuz.", MType = "error", Title = "Hata" });
                }
            }
            catch (Exception ex)
            {
                return Json(new ResultJson { Message = "Bir Hata Oluştu :" +ex, MType = "error", Title = "Hata" });
            }
        }

        [HttpGet]
        public ActionResult Guncelle(int ID)
        {
            var kategori = _kategoriRepository.GetById(ID);
            if (kategori == null)
            {
                throw new Exception("Kategori kayıt bulunamadı.");
            }
            return View(new ViewModelKategori() { ID = kategori.ID, Ad = kategori.Ad, Durumu = kategori.AktifMi, Siralama = kategori.Siralama });
        }

        [HttpPost]
        public JsonResult Guncelle(ViewModelKategori viewModelKategori)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _kategoriRepository.Update(PreEkleGuncelle(viewModelKategori));
                    _kategoriRepository.Save();

                    return Json(
                      new ResultJson
                      {
                          Message = "Kategori güncelleme işlemi başarılı.",
                          MType = "success",
                          Title = "Kategori Güncellendi",
                          RedirectURL = "/KizilayAdmin/Kategori/Listele"
                      });
                }
                else
                {
                    return Json(new ResultJson { Message = "Lütfen zorunlu alanları doldurunuz.", MType = "error", Title = "Hata" });
                }
            }
            catch (Exception ex)
            {
                return Json(new ResultJson { Message = "Bir Hata Oluştu :" + ex, MType = "error", Title = "Hata" });
            }
        }

        [HttpPost]
        public ActionResult Sil(int ID)
        {
            var kategori = _kategoriRepository.GetById(ID);
            if (kategori == null)
            {
                return Json(new ResultJson { Message = "Kategori silmede kayıt bulunamadı", MType = "error", Title = "Hata" });
            }
            _kategoriRepository.Delete(kategori);
            _kategoriRepository.Save();

            return Json(new ResultJson { Message = "Kategori silme işlemi başarılı.", MType = "success", Title = "Kategori Silindi" });
        }

        #region PreEkleGuncelle Kategori
        public Kategori PreEkleGuncelle(ViewModelKategori viewModelKategori)
        {
            var kategori = new Kategori();
            var sessionID = Session["KullaniciAdi"].ToString();
            var kullaniciID= _kullaniciRepository.Get(x => x.KullaniciAdi == sessionID && x.AktifMi == true).ID;
            if (viewModelKategori.ID == 0)
            {
                kategori.EklenmeTarihi = DateTime.Now;
                kategori.Siralama = _kategoriRepository.Count() == 0 ? 1 : _kategoriRepository.GetAll().OrderByDescending(q => q.Siralama).Max(q => q.Siralama) + 1;
            }
            else
            {
                kategori = _kategoriRepository.GetById(viewModelKategori.ID);
                kategori.DegistirmeTarihi = DateTime.Now;
                kategori.GuncelleyenKullaniciId = kullaniciID;
                kategori.Versiyon = kategori.Versiyon + 1;
            }

            kategori.Ad = viewModelKategori.Ad;
            kategori.AktifMi = viewModelKategori.Durumu;
            kategori.SeoSlug = StringManager.ToSlug(viewModelKategori.Ad);
            kategori.EkleyenKullaniciId = kullaniciID;

            return kategori;
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            _kategoriRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}