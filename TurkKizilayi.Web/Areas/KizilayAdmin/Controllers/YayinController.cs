﻿using Omu.Drawing;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data;
using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;
using TurkKizilayi.Web.CustomFilter;
using Altyapi.Utils;

namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    [LoginFilter]
    public class YayinController : Controller
    {
        #region Constructor
        private readonly IYayinRepository _yayinRepository;
        private readonly IKategoriRepository _kategoriRepository;
        private readonly IKullaniciRepository _kullaniciRepository;
        private readonly IResimRepository _resimRepository;

        public YayinController(IYayinRepository yayinRepository, IKategoriRepository kategoriRepository, IKullaniciRepository kullaniciRepository, IResimRepository resimRepository)
        {
            _yayinRepository = yayinRepository;
            _kategoriRepository = kategoriRepository;
            _kullaniciRepository = kullaniciRepository;
            _resimRepository= resimRepository;
        }
        #endregion

        [HttpGet]
        public ActionResult Listele()
        {
            var viewModelYayinlar = from yayin in _yayinRepository.GetAll().AsEnumerable()
                                 orderby yayin.ID descending
                                 select new ViewModelYayin
                                 {
                                     ID = yayin.ID,
                                     KategoriID = yayin.KategoriId,
                                     KategoriAdi=yayin.Kategori.Ad,
                                     Baslik = yayin.Baslik,
                                     Icerik = yayin.Icerik,
                                     KisaAciklama = yayin.KisaAciklama,
                                     ResimKucuk = yayin.ResimUrlKucuk,
                                     ResimBuyuk = yayin.ResimUrlBuyuk,
                                     Resimler=yayin.Resimler.ToList(),
                                     Durumu = yayin.AktifMi,
                                     SlayttaGorunsunMu = yayin.SlayttaGorunsunMu,
                                     Siralama = yayin.Siralama,
                                     YayinTarihi= yayin.YayinTarihi
                                 };
            KategoriListesiGetir();
            return View(viewModelYayinlar);
        }

        [HttpGet]
        public ActionResult Ekle()
        {
            KategoriListesiGetir();
            return View(new ViewModelYayin() {YayinTarihi = DateTime.Now});
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Ekle(ViewModelYayin viewModelYayin)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModelYayin.BaslikResim != null)
                    {
                        var resimPaketi = ImageUtils.ResimBoyutlaKaydet(viewModelYayin.BaslikResim, "/External/Yayin/");
                        viewModelYayin.ResimKucuk = resimPaketi[0];
                        viewModelYayin.ResimBuyuk = resimPaketi[1];
                    }

                    if (viewModelYayin.Galeri.Count() != 0)
                    {
                        viewModelYayin.Resimler = ImageUtils.GaleriBoyutlaKaydet(viewModelYayin.Galeri);
                    }

                    _yayinRepository.Insert(PreEkleGuncelle(viewModelYayin));
                    _yayinRepository.Save();

                    return RedirectToAction("Listele");
                }
                return RedirectToAction("Ekle");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Listele");
            }
        }

        [HttpGet]
        public ActionResult Guncelle(int ID)
        {
            var yayin = _yayinRepository.GetById(ID);
            if (yayin == null)
            {
                throw new Exception("Yayin kayıt bulunamadı.");
            }
            KategoriListesiGetir();
            return View(
                new ViewModelYayin()
                {
                    ID = yayin.ID,
                    Baslik = yayin.Baslik,
                    KisaAciklama = yayin.KisaAciklama,
                    Icerik = yayin.Icerik,
                    KategoriID = yayin.KategoriId,
                    KategoriAdi = yayin.Kategori.Ad,
                    ResimKucuk = yayin.ResimUrlKucuk,
                    ResimBuyuk = yayin.ResimUrlBuyuk,
                    Resimler = yayin.Resimler.ToList(),
                    Durumu = yayin.AktifMi,
                    SlayttaGorunsunMu = yayin.SlayttaGorunsunMu,
                    Siralama = yayin.Siralama,
                    YayinTarihi = yayin.YayinTarihi
                });
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Guncelle(ViewModelYayin viewModelYayin)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModelYayin.ResimBuyuk != null)
                    {
                        if (viewModelYayin.BaslikResim!=null)
                        {
                            ImageUtils.ResimFizikselSil(new Resim() {ResimUrlBuyuk = viewModelYayin.ResimBuyuk,ResimUrlKucuk = viewModelYayin.ResimKucuk});
                            var resimPaketi= ImageUtils.ResimBoyutlaKaydet(viewModelYayin.BaslikResim, "/External/Yayin/");
                            viewModelYayin.ResimKucuk = resimPaketi[0];
                            viewModelYayin.ResimBuyuk = resimPaketi[1];
                        }
                    }

                    if (viewModelYayin.Galeri.Count() != 0)
                    {
                        viewModelYayin.Resimler = ImageUtils.GaleriBoyutlaKaydet(viewModelYayin.Galeri);
                    }
                    
                    _yayinRepository.Update(PreEkleGuncelle(viewModelYayin));
                    _yayinRepository.Save();

                    return RedirectToAction("Listele");
                }
                return RedirectToAction("Guncelle");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Listele");
            }
        }

        [HttpPost]
        public ActionResult Sil(int ID)
        {
            var yayin = _yayinRepository.GetById(ID);
            if (yayin == null)
            {
                return Json(new ResultJson { Message = "Yayın silmede kayıt bulunamadı", MType = "error", Title = "Hata" });
            }
            YayinResimSil(yayin);
            _yayinRepository.Delete(yayin);
            _yayinRepository.Save();

            return Json(new ResultJson { Message = "Yayın silme işlemi başarılı.", MType = "success", Title = "Yayın Silindi" });
        }

        [HttpPost]
        public JsonResult ResimSil(int ID)
        {
            var silinecekResim = _resimRepository.GetById(ID);

            ImageUtils.ResimFizikselSil(silinecekResim);
            _resimRepository.Delete(silinecekResim);
            _resimRepository.Save();

            return Json(new ResultJson { Message = "Resim silme işlemi başarılı.", MType = "success", Title = "Resim Silindi" });
        }


        #region KategoriListesi

        public void KategoriListesiGetir()
        {
            var kategoriListesi = _kategoriRepository.GetAll().AsEnumerable().Select(q=>new { Ad=q.Ad,ID=q.ID});
            ViewBag.kategoriListesi = kategoriListesi;
        }

        #endregion

        #region PreEkleGuncelle Yayin
        public Yayin PreEkleGuncelle(ViewModelYayin viewModelYayin)
        {
            var yayin = new Yayin();
            var sessionID = Session["KullaniciAdi"].ToString();
            var kullaniciID = _kullaniciRepository.Get(x => x.KullaniciAdi == sessionID && x.AktifMi == true).ID;
            if (viewModelYayin.ID == 0)
            {
                yayin.EklenmeTarihi = DateTime.Now;
                yayin.Siralama = _yayinRepository.Count() == 0 ? 1 : _yayinRepository.GetAll().OrderByDescending(q => q.Siralama).Max(q => q.Siralama) + 1;
            }
            else
            {
                yayin = _yayinRepository.GetById(viewModelYayin.ID);
                yayin.DegistirmeTarihi = DateTime.Now;
                yayin.GuncelleyenKullaniciId = kullaniciID;
                yayin.Versiyon = yayin.Versiyon + 1;                
            }

            viewModelYayin.Resimler.ForEach(q => yayin.Resimler.Add(q));
            yayin.Baslik = viewModelYayin.Baslik;
            yayin.KisaAciklama = viewModelYayin.KisaAciklama;
            yayin.Icerik = viewModelYayin.Icerik;
            yayin.AktifMi = viewModelYayin.Durumu;
            yayin.SlayttaGorunsunMu = viewModelYayin.SlayttaGorunsunMu;
            yayin.KategoriId = viewModelYayin.KategoriID;
            yayin.SeoSlug = StringManager.ToSlug(viewModelYayin.Baslik);
            yayin.EkleyenKullaniciId = kullaniciID;
            yayin.YayinTarihi = viewModelYayin.YayinTarihi;
            if (viewModelYayin.ResimBuyuk != null || viewModelYayin.ResimBuyuk != "")
            {
                yayin.ResimUrlBuyuk = viewModelYayin.ResimBuyuk;
                yayin.ResimUrlKucuk = viewModelYayin.ResimKucuk;
            }
            
            return yayin;
        }
        #endregion


        #region YayinResim Sil
        public void YayinResimSil(Yayin yayin)
        {
            ImageUtils.ResimFizikselSil(new Resim { ResimUrlBuyuk = yayin.ResimUrlBuyuk, ResimUrlKucuk = yayin.ResimUrlKucuk });

            if (!yayin.Resimler.Any()) return;
            foreach (var resim in yayin.Resimler)
            {
                ImageUtils.ResimFizikselSil(resim);
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            _yayinRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}