﻿using System.Web.Mvc;
using TurkKizilayi.Web.CustomFilter;

namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    [LoginFilter]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}