﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data;
using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;
using Omu.Drawing;
using TurkKizilayi.Web.CustomFilter;
using Altyapi.Utils;

namespace TurkKizilayi.Web.Areas.KizilayAdmin.Controllers
{
    [LoginFilter]
    public class KullaniciController : Controller
    {
        #region Constructor
        private readonly IKullaniciRepository _kullaniciRepository;

        public KullaniciController( IKullaniciRepository kullaniciRepository)
        {
            _kullaniciRepository = kullaniciRepository;
        }
        #endregion

        public ActionResult Listele()
        {
            var viewModelKullanicilar = from kullanici in _kullaniciRepository.GetAll().AsEnumerable()
                                    orderby kullanici.ID descending
                                    select new ViewModelKullanici
                                    {
                                        ID = kullanici.ID,
                                        KullaniciAdi = kullanici.KullaniciAdi,
                                        AdiSoyadi = kullanici.AdSoyad,
                                        ResimKucuk = kullanici.ResimKucuk,
                                        ResimBuyuk = kullanici.ResimBuyuk,
                                        Durumu = kullanici.AktifMi,
                                        Roller = kullanici.Roller.ToList()
                                    };

            return View(viewModelKullanicilar);
        }

        [HttpGet]
        public ActionResult Ekle()
        {
            return View(new ViewModelKullanici());
        }

        [HttpPost]
        public ActionResult Ekle(ViewModelKullanici viewModelKullanici)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModelKullanici.ResimBuyuk != null || viewModelKullanici.ResimBuyuk != "")
                    {
                        var kullaniciResimPaket = ImageUtils.ResimBoyutlaKaydet(viewModelKullanici.KullaniciResim, "/External/Kullanici/");
                        viewModelKullanici.ResimKucuk = kullaniciResimPaket[0];
                        viewModelKullanici.ResimBuyuk = kullaniciResimPaket[1];
                    }

                    _kullaniciRepository.Insert(PreEkleGuncelle(viewModelKullanici));
                    _kullaniciRepository.Save();

                    return RedirectToAction("Listele");
                }
                return RedirectToAction("Ekle");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Listele");
            }
        }

        [HttpGet]
        public ActionResult Guncelle(int ID)
        {
            var kullanici = _kullaniciRepository.GetById(ID);
            if (kullanici == null)
            {
                throw new Exception("Kullanıcı kayıt bulunamadı.");
            }
            return View(
                new ViewModelKullanici()
                {
                    ID = kullanici.ID,
                    KullaniciAdi = kullanici.KullaniciAdi,
                    AdiSoyadi = kullanici.AdSoyad,
                    Eposta = kullanici.Eposta,
                    ResimKucuk = kullanici.ResimKucuk,
                    ResimBuyuk = kullanici.ResimBuyuk,
                    Roller = kullanici.Roller.ToList(),
                    Durumu = kullanici.AktifMi,
                });
        }

        [HttpPost]
        public ActionResult Guncelle(ViewModelKullanici viewModelKullanici)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModelKullanici.KullaniciResim != null)
                    {
                        if (viewModelKullanici.ResimBuyuk != null)
                        {
                            ImageUtils.ResimFizikselSil(new Resim() { ResimUrlBuyuk = viewModelKullanici.ResimBuyuk, ResimUrlKucuk = viewModelKullanici.ResimKucuk });
                        }
                        var kullaniciResimPaket = ImageUtils.ResimBoyutlaKaydet(viewModelKullanici.KullaniciResim, "/External/Kullanici/");
                        viewModelKullanici.ResimKucuk = kullaniciResimPaket[0];
                        viewModelKullanici.ResimBuyuk = kullaniciResimPaket[1];
                    }

                    _kullaniciRepository.Update(PreEkleGuncelle(viewModelKullanici));
                    _kullaniciRepository.Save();
                    Session["Kullanici"] = _kullaniciRepository.GetById(viewModelKullanici.ID);
                    return RedirectToAction("Listele");
                }
                return RedirectToAction("Ekle");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Listele");
            }
        }

        [HttpGet]
        public ActionResult SifreDegistir(int ID)
        {
            var viewKullaniciSifre =new ViewModelKullaniciSifre() {ID = ID};
            return View(viewKullaniciSifre);
        }

        [HttpPost]
        public ActionResult SifreDegistir(ViewModelKullaniciSifre viewModelKullaniciSifre)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var kullanici = _kullaniciRepository.GetById(viewModelKullaniciSifre.ID);
                    viewModelKullaniciSifre.SetPassword(viewModelKullaniciSifre.YeniSifre);
                    kullanici.Sifre = viewModelKullaniciSifre.YeniSifre;
                    _kullaniciRepository.Update(kullanici);
                    _kullaniciRepository.Save();

                    return RedirectToAction("Index","Anasayfa");
                }
                return RedirectToAction("SifreDegistir");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Anasayfa");
            }
        }

        #region PreEkleGuncelle Kullanıcı
        public Kullanici PreEkleGuncelle(ViewModelKullanici viewModelKullanici)
        {
            var kullanici = new Kullanici();
            if (viewModelKullanici.ID == 0)
            {
                kullanici.EklenmeTarihi = DateTime.Now;
                kullanici.SetPassword(WebConfigurationManager.AppSettings["VarsayilanSifre"]);
            }
            else
            {
                kullanici = _kullaniciRepository.GetById(viewModelKullanici.ID);
                kullanici.DegistirmeTarihi = DateTime.Now;
            }

            kullanici.KullaniciAdi = viewModelKullanici.KullaniciAdi;
            kullanici.AdSoyad = viewModelKullanici.AdiSoyadi;
            kullanici.Eposta = viewModelKullanici.Eposta;
            kullanici.ResimKucuk = viewModelKullanici.ResimKucuk;
            kullanici.ResimBuyuk = viewModelKullanici.ResimBuyuk;
            kullanici.AktifMi = viewModelKullanici.Durumu;
            kullanici.SeoSlug = StringManager.ToSlug(viewModelKullanici.KullaniciAdi);

            return kullanici;
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            _kullaniciRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}