﻿using System.Web.Mvc;

namespace TurkKizilayi.Web.Areas.KizilayAdmin
{
    public class KizilayAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "KizilayAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "KizilayAdmin_default",
                "KizilayAdmin/{controller}/{action}/{id}",
                new { controller = "Account", action = "Login", id = UrlParameter.Optional },
                new[] { "TurkKizilayi.Web.Areas.KizilayAdmin.Controllers" }
            );
        }
    }
}