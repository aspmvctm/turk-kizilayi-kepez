﻿using System.Web.Optimization;

namespace TurkKizilayi.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css")
                .Include("~/Content/bootstrap.min.css",
                "~/Scripts/simple-line-icons/css/simple-line-icons.css",
                "~/Scripts/owl.carousel/assets/owl.carousel.min.css",
                "~/Scripts/owl.carousel/assets/owl.theme.default.min.css",
                "~/Scripts/magnific-popup/magnific-popup.css",
                "~/Content/css/theme.css",
                "~/Content/css/theme-elements.css",
                "~/Content/css/theme-blog.css",
                "~/Content/css/theme-animate.css",
                "~/Scripts/rs-plugin/css/settings.css",
                "~/Scripts/rs-plugin/css/layers.css",
                "~/Scripts/rs-plugin/css/navigation.css",
                "~/Scripts/circle-flip-slideshow/css/component.css",
                "~/Scripts/pnotify/pnotify.css",
                "~/Content/css/skins/default.css",
                "~/Content/css/custom.css")
                );

            bundles.Add(new ScriptBundle("~/adminjs")
                .Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/js")
                .Include("~/Scripts/modernizr/modernizr.js",
                "~/Scripts/jquery/jquery.js",
                "~/Scripts/jquery.appear/jquery.appear.js",
                "~/Scripts/jquery.easing/jquery.easing.js",
                "~/Scripts/jquery-cookie/jquery-cookie.js",
                "~/Scripts/bootstrap/js/bootstrap.min.js",
                "~/Scripts/common/common.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/owl.carousel/owl.carousel.min.js",
                "~/Scripts/magnific-popup/jquery.magnific-popup.js",
                "~/Scripts/pnotify/pnotify.js",
                "~/Scripts/jquery.form.min.js",
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/theme.js",
                "~/Scripts/rs-plugin/js/jquery.themepunch.tools.min.js",
                "~/Scripts/rs-plugin/js/jquery.themepunch.revolution.min.js",
                "~/Scripts/circle-flip-slideshow/js/jquery.flipshow.js",
                "~/Scripts/views/view.home.js",
                "~/Scripts/custom.js",
                "~/Scripts/theme.init.js"));

            bundles.Add(new StyleBundle("~/admincss").Include(
          "~/Content/bootstrap.min.css",
          "~/Content/site.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
