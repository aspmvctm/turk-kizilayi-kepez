﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TurkKizilayi.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "YayinDetay",
                url: "{slugKategori}/detay/{slugUrl}",
                defaults: new {controller = "Yayin", action = "YayinDetay"},
                namespaces: new[] {"TurkKizilayi.Web.Controllers"}
                );

            routes.MapRoute(
                name: "Kategoriler",
                url: "kategoriler/{slugKategori}",
                defaults: new {controller = "Yayin", action = "Kategori"},
                namespaces: new[] {"TurkKizilayi.Web.Controllers"}
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Anasayfa", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "TurkKizilayi.Web.Controllers" }
            );


        }
    }
}
