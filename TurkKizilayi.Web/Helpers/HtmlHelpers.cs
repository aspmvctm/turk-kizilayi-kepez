﻿using System.Web.Mvc;

namespace TurkKizilayi.Web.Helpers
{
    public static class HtmlHelpers
    {
        public static string ActivePage(this HtmlHelper helper,  string action,string controller)
        {
            string classValue = "";
            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
            if (currentController == controller && currentAction == action)
            {
                classValue = "active";
            }
            return classValue;
        }

        public static string ActivePageWithKategori(this HtmlHelper helper, string action, string controller, string slugKategori)
        {
            string classValue = "";
            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();

            if (currentController == controller && currentAction == action)
            {
                string currentKategori = helper.ViewContext.Controller.ValueProvider.GetValue("slugKategori").RawValue.ToString();
                if (currentKategori != "")
                {
                    if (currentKategori == slugKategori)
                    {
                        classValue = "active";
                    }
                }

            }
            return classValue;
        }

        public static string ActivePageWithDropdown(this HtmlHelper helper,string controller)
        {
            string classValue = "";
            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            if (currentController == controller)
            {
                classValue = "active";
            }
            return classValue;
        }
    }
}