﻿$(document).ready(function(){
    if (localStorage.getItem("dataCrud")) {
        var pNotify = new PNotify({
            title: localStorage.getItem("Title"),
            text: localStorage.getItem("Message"),
            type: localStorage.getItem("MType"),
            delay: 3000
        });
    }
    localStorage.clear();
});

$(document).on("click", " #YayinSil", function () {
    event.preventDefault();
    var gelenID = $(this).attr("data-id");
    var tableParentTr = $(this).closest("tr");
    $.ajax({
        url: '/KizilayAdmin/Yayin/Sil/' + gelenID,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            var pNotify;
            if (response.MType === "success") {
                tableParentTr.fadeOut(500, function () {
                    tableParentTr.remove();
                });
                pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
            else {
                pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
        }
    });
});
$(document).on("click", " #KategoriSil", function () {
    event.preventDefault();
    var gelenID = $(this).attr("data-id");
    var tableParentTr = $(this).closest("tr");
    $.ajax({
        url: '/KizilayAdmin/Kategori/Sil/' + gelenID,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            var pNotify;
            if (response.MType === "success") {
                tableParentTr.fadeOut(500, function () {
                    tableParentTr.remove();
                });
                pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
            else {
                pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
        }
    });
});
$(document).on("click", " #KullaniciSil", function () {
    event.preventDefault();
    var gelenID = $(this).attr("data-id");
    var tableParentTr = $(this).closest("tr");
    $.ajax({
        url: '/KizilayAdmin/Kullanici/Sil/' + gelenID,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.MType === "success") {
                tableParentTr.fadeOut(500, function () {
                    tableParentTr.remove();
                });
                var pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
            else {
                var notify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
        }
    });
});

$(document).on("click", " #RolSil", function () {
    event.preventDefault();
    var gelenID = $(this).attr("data-id");
    var tableParentTr = $(this).closest("tr");
    $.ajax({
        url: '/KizilayAdmin/Rol/Sil/' + gelenID,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.MType === "success") {
                tableParentTr.fadeOut(500, function () {
                    tableParentTr.remove();
                });
                var pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
            else {
                var notify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
        }
    });
});

$(document).on("click", " #ResimSil", function () {
    event.preventDefault();
    var gelenID = $(this).attr("data-id");
    var tableParentTr = $(this).closest("div");
    $.ajax({
        url: '/KizilayAdmin/Yayin/ResimSil/' + gelenID,
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.MType === "success") {
                tableParentTr.fadeOut(500, function () {
                    tableParentTr.remove();
                });
                var pNotify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
            else {
                var notify = new PNotify({
                    title: response.Title,
                    text: response.Message,
                    type: response.MType,
                    delay: 3000
                });
            }
        }
    });
});

function OnSuccessCrud(data, status, xhr) {
    var response = data;
    if (response.MType === "success") {
        localStorage.setItem("dataCrud", true);
        localStorage.setItem("Title", response.Title);
        localStorage.setItem("Message", response.Message);
        localStorage.setItem("MType", response.MType);
        window.location.replace(response.RedirectURL);
    }
    else
    {
        $.each(data, function (i, item) {
            var pNotify = new PNotify({
                title: data[i].Title,
                text: data[i].Message,
                type: data[i].MType,
                delay: 3000
            });
        });
    }
}


function ValidateForm() {
    return $('form').validate().form();
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImg').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("input[name*='BaslikResim']").change(function () {
    readURL(this);
});

$("input[name*='KullaniciResim']").change(function () {
    readURL(this);
});


window.onload = function () {

    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
        var filesInput = document.getElementById("Galeri");

        if (filesInput) {

            filesInput.addEventListener("change",
                function(event) {

                    var files = event.target.files; //FileList object
                    var output = document.getElementById("result");

                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];

                        //Only pics
                        if (!file.type.match('image'))
                            continue;

                        var picReader = new FileReader();

                        picReader.addEventListener("load",
                            function(event) {

                                var picFile = event.target;

                                var div = document.createElement("div");

                                div.style.margin = "1%";
                                div.style.float = "left";
                                div.style.width = "23%";

                                //div.innerHTML = "<img class='thumbnail img-responsive' src='" + picFile.result + "'" +
                                //        "title='" + picFile.name + "'/> <a href='#' class='remove_pict'>X</a>";

                                //output.insertBefore(div, null);
                                //div.children[1].addEventListener("click", function (event) {
                                //    div.parentNode.removeChild(div);
                                //    var gelenID = $(div.children[1]).attr("data-id");
                                //    $.ajax({
                                //        url: '/Yayin/ResimSil/' + gelenID,
                                //        type: 'POST',
                                //        dataType: 'json',
                                //        success: function () {
                                //            div.parentNode.removeChild(div);
                                //        }
                                //    })
                                //})

                                div
                                    .innerHTML =
                                    "<a class='remove_pict fa fa-times fa-2x' style='position:relative;left: -10px;top: 15px;color: red;cursor:pointer;'></a><img class='thumbnail img-responsive' style='margin-bottom:0px;' src='" +
                                    picFile.result +
                                    "'" +
                                    "title='" +
                                    picFile.name +
                                    "'/> ";

                                output.insertBefore(div, null);
                                div.children[1].addEventListener("click",
                                    function(event) {
                                        div.parentNode.removeChild(div);
                                        var gelenID = $(div.children[1]).attr("data-id");
                                        $.ajax({
                                            url: '/Yayin/ResimSil/' + gelenID,
                                            type: 'POST',
                                            dataType: 'json',
                                            success: function() {
                                                div.parentNode.removeChild(div);
                                            }
                                        })
                                    })
                            })

                        //Read the image
                        picReader.readAsDataURL(file);
                    }

                })
        } else {
            console.log("Your browser does not support File API");
        }
    }
}


