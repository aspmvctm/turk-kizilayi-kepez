﻿var map;

function initMap() {
    // Create a map object and specify the DOM element for display.
    var latlng = new google.maps.LatLng(36.906476, 30.681079);
     map = new google.maps.Map(document.getElementById('googlemaps'), {
         center: latlng,
         controls: {
             draggable: (($.browser.mobile) ? false : true),
             panControl: true,
             zoomControl: true,
             mapTypeControl: true,
             scaleControl: true,
             streetViewControl: true,
             overviewMapControl: true
         },
         scrollwheel: true,
         zoom: 16
     });

     var marker = new google.maps.Marker({
         position: { lat: 36.906476, lng: 30.681079 },
         title: 'Turk Kizilayi Kepez Subesi',
         map: map,
         popup:true
     });
 }

 function OnSuccessCrud(data, status, xhr) {
     var response = data;
     if (response.MType === "success") {
         var pNotify = new PNotify({
             title: response.Title,
             text: response.Message,
             type: response.MType,
             delay: 3000
         });
         $(':input')
             .not(':button, :submit, :reset, :hidden')
             .removeAttr('checked')
             .removeAttr('selected')
             .not('‌​:checkbox, :radio, select')
             .val('');
     } else {
         var pNotify = new PNotify({
             title: response.Title,
             text: response.Message,
             type: response.MType,
             delay: 3000
         });
     }
 }


 function OnCompleteCrud(data) {
     var response = data;
     if (response.MType === "success") {
         var pNotify = new PNotify({
             title: response.Title,
             text: response.Message,
             type: response.MType,
             delay: 3000
         });
     }
 }

 function ValidateForm() {
     return $('form').validate().form();
 }

