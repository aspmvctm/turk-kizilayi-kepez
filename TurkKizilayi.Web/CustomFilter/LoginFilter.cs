﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TurkKizilayi.Web.CustomFilter
{
    public class LoginFilter : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var wrapper = new HttpContextWrapper(HttpContext.Current);
            var sessionControl = filterContext.HttpContext.Session["KullaniciAdi"];
            if (sessionControl == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "controller", "Account" }, { "action", "Login" }, { "namespaces", "TurkKizilayi.Web.Areas.KizilayAdmin.Controllers" } });
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}