﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Configuration;
using System.Web.Mvc;
using Altyapi.Data;
using Altyapi.Data.ViewModel;

namespace TurkKizilayi.Web.Controllers
{
    public class IletisimController : Controller
    {
        // GET: Iletisim

        [HttpGet]
        public ActionResult BizeUlasin()
        {
            return View(new ViewModelIletisimFormu());
        }

        [HttpPost]
        public JsonResult BizeUlasin(ViewModelIletisimFormu viewModelIletisimFormu)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var mailMessage = new MailMessage();
                    var senderMail = new MailAddress(WebConfigurationManager.AppSettings["KurumsalEposta"]);
                    var sb = new StringBuilder();
                    mailMessage.To.Add(WebConfigurationManager.AppSettings["KurumsalEposta"]);
                    mailMessage.From = senderMail;
                    mailMessage.Subject = viewModelIletisimFormu.Konu;
                    mailMessage.IsBodyHtml = false;
                    
                    //smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    sb.Append("Adı Soyadı: " + viewModelIletisimFormu.Ad);
                    sb.Append(Environment.NewLine);
                    sb.Append("E-Posta: " + viewModelIletisimFormu.Eposta);
                    sb.Append(Environment.NewLine);
                    sb.Append(Environment.NewLine);
                    sb.Append("Mesaj: " + viewModelIletisimFormu.Mesaj);
                    mailMessage.Body = sb.ToString();
                    using (var smtp = new SmtpClient())
                    {
                        smtp.EnableSsl = false;
                        smtp.UseDefaultCredentials = false;
                        smtp.Host = WebConfigurationManager.AppSettings["SmtpHost"];
                        smtp.Port = int.Parse(WebConfigurationManager.AppSettings["SmtpPort"]);
                        smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["KurumsalEposta"], WebConfigurationManager.AppSettings["KurumsalEpostaSifre"]);
                        smtp.Send(mailMessage);
                    }
                    
                    return Json(
                       new ResultJson
                       {
                           Message = "Mesaj Gönderme İşlemi Başarılı.",
                           MType = "success",
                           Title = "Mesaj Gönderildi",
                       });
                }
                catch (Exception ex)
                {
                    return Json(new ResultJson { Message = "Bir Hata Oluştu", MType = "error", Title = "Hata" });
                }
            }
            else
            {
                return Json(new ResultJson { Message = "Lütfen zorunlu alanları doldurunuz.", MType = "error", Title = "Hata" });
            }

        }
    }
}