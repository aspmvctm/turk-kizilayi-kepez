﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.Model;
using Altyapi.Data.ViewModel;

namespace TurkKizilayi.Web.Controllers
{
    public class AnasayfaController : Controller
    {
        #region Constructor
        private readonly IYayinRepository _yayinRepository;

        public AnasayfaController(IYayinRepository yayinRepository)
        {
            _yayinRepository = yayinRepository;
        }
        #endregion

        public ActionResult Index()
        {
            var yayinlar = _yayinRepository.GetAll().Where(q => q.AktifMi);
            var enumerable = yayinlar as IList<Yayin> ?? yayinlar.ToList();
            var viewModelAnasayfa = new ViewModelAnasayfa
            {
                ViewModelYayin = (from yayin in enumerable.Take(5)
                                  orderby yayin.YayinTarihi descending
                                  select new ViewModelYayin
                                  {
                                      ID = yayin.ID,
                                      KategoriID = yayin.KategoriId,
                                      KategoriAdi = yayin.Kategori.Ad,
                                      SeoSlugKategori = yayin.Kategori.SeoSlug,
                                      Baslik = yayin.Baslik,
                                      Icerik = yayin.Icerik,
                                      KisaAciklama = yayin.KisaAciklama,
                                      ResimKucuk = yayin.ResimUrlKucuk,
                                      ResimBuyuk = yayin.ResimUrlBuyuk,
                                      Resimler = yayin.Resimler.ToList(),
                                      Durumu = yayin.AktifMi,
                                      Siralama = yayin.Siralama,
                                      SeoSlug = yayin.SeoSlug,
                                      YayinTarihi = yayin.YayinTarihi
                                  }),

                ViewModelSlaytYayin = (from yayin in enumerable.Where(q => q.SlayttaGorunsunMu && q.AktifMi)
                                       orderby yayin.YayinTarihi descending
                                       select new ViewModelSlaytYayin
                                       {
                                           ID = yayin.ID,
                                           KategoriID = yayin.KategoriId,
                                           KategoriAdi = yayin.Kategori.Ad,
                                           SeoSlugKategori = yayin.Kategori.SeoSlug,
                                           Baslik = yayin.Baslik,
                                           KisaAciklama = yayin.KisaAciklama,
                                           ResimKucuk = yayin.ResimUrlKucuk,
                                           ResimBuyuk = yayin.ResimUrlBuyuk,
                                           Siralama = yayin.Siralama,
                                           SeoSlug = yayin.SeoSlug,
                                           YayinTarihi = yayin.YayinTarihi
                                       })
            };

            return View(viewModelAnasayfa);
        }
    }
}