﻿using System.Linq;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.ViewModel;

namespace TurkKizilayi.Web.Controllers
{
    public class YayinController : Controller
    {
        #region Constructor
        private readonly IYayinRepository _yayinRepository;
        private readonly IKategoriRepository _kategoriRepository;

        public YayinController(IYayinRepository yayinRepository, IKategoriRepository kategoriRepository)
        {
            _yayinRepository = yayinRepository;
            _kategoriRepository = kategoriRepository;
        }
        #endregion

        // GET: Yayin
        public ActionResult Kategori(string slugKategori)
        {
            var viewModelYayin = (from yayin in _yayinRepository.
                                  GetMany(q => q.Kategori.SeoSlug == slugKategori)
                orderby yayin.YayinTarihi descending
                select new ViewModelYayin
                {
                    ID = yayin.ID,
                    KategoriID = yayin.KategoriId,
                    KategoriAdi = yayin.Kategori.Ad,
                    SeoSlugKategori = yayin.Kategori.SeoSlug,
                    Baslik = yayin.Baslik,
                    Icerik = yayin.Icerik,
                    KisaAciklama = yayin.KisaAciklama,
                    ResimKucuk = yayin.ResimUrlKucuk,
                    ResimBuyuk = yayin.ResimUrlBuyuk,
                    Resimler = yayin.Resimler.ToList(),
                    Durumu = yayin.AktifMi,
                    Siralama = yayin.Siralama,
                    SeoSlug = yayin.SeoSlug,
                    YayinTarihi = yayin.YayinTarihi
                });
            ViewBag.KategoriAdi = _kategoriRepository.Get(q => q.SeoSlug == slugKategori).Ad;
            return View(viewModelYayin);
        }

        public ActionResult Ziyaretler()
        {
            var viewModelYayin = (from yayin in _yayinRepository.
                                  GetMany(q => q.Kategori.SeoSlug == "ziyaretler")
                                  orderby yayin.YayinTarihi descending
                                  select new ViewModelYayin
                                  {
                                      ID = yayin.ID,
                                      KategoriID = yayin.KategoriId,
                                      KategoriAdi = yayin.Kategori.Ad,
                                      SeoSlugKategori = yayin.Kategori.SeoSlug,
                                      Baslik = yayin.Baslik,
                                      Icerik = yayin.Icerik,
                                      KisaAciklama = yayin.KisaAciklama,
                                      ResimKucuk = yayin.ResimUrlKucuk,
                                      ResimBuyuk = yayin.ResimUrlBuyuk,
                                      Resimler = yayin.Resimler.ToList(),
                                      Durumu = yayin.AktifMi,
                                      Siralama = yayin.Siralama,
                                      SeoSlug = yayin.SeoSlug,
                                      YayinTarihi = yayin.YayinTarihi
                                  });
            ViewBag.KategoriAdi = _kategoriRepository.Get(q => q.SeoSlug == "ziyaretler").Ad;
            return View(viewModelYayin);
        }

        public ActionResult YayinDetay(string slugUrl,string slugKategori)
        {
            var cyayin =  _yayinRepository.Get(q => q.SeoSlug == slugUrl&&q.Kategori.SeoSlug==slugKategori);
            var viewModelYayinlar = (from yayin in _yayinRepository.GetAll()
                                  orderby yayin.YayinTarihi descending
                                  select new ViewModelYayin
                                  {
                                      ID = yayin.ID,
                                      KategoriID = yayin.KategoriId,
                                      KategoriAdi = yayin.Kategori.Ad,
                                      SeoSlugKategori = yayin.Kategori.SeoSlug,
                                      Baslik = yayin.Baslik,
                                      Icerik = yayin.Icerik,
                                      KisaAciklama = yayin.KisaAciklama,
                                      ResimKucuk = yayin.ResimUrlKucuk,
                                      ResimBuyuk = yayin.ResimUrlBuyuk,
                                      Resimler = yayin.Resimler.ToList(),
                                      Durumu = yayin.AktifMi,
                                      Siralama = yayin.Siralama,
                                      SeoSlug = yayin.SeoSlug,
                                      YayinTarihi = yayin.YayinTarihi
                                  }).Take(3);
            ViewBag.SonYayinlar = viewModelYayinlar;
            ViewBag.KategoriAdi = _kategoriRepository.Get(q => q.SeoSlug == slugKategori).Ad;
            if (cyayin == null)
            {
                return View(new ViewModelYayin());
            }
            else
            {
                var viewModelYayin = new ViewModelYayin
                {
                    ID = cyayin.ID,
                    KategoriID = cyayin.KategoriId,
                    KategoriAdi = cyayin.Kategori.Ad,
                    Baslik = cyayin.Baslik,
                    Icerik = cyayin.Icerik,
                    KisaAciklama = cyayin.KisaAciklama,
                    ResimKucuk = cyayin.ResimUrlKucuk,
                    ResimBuyuk = cyayin.ResimUrlBuyuk,
                    Resimler = cyayin.Resimler.ToList(),
                    Durumu = cyayin.AktifMi,
                    Siralama = cyayin.Siralama,
                    SeoSlug = cyayin.SeoSlug,
                    YayinTarihi = cyayin.YayinTarihi
                };
                return View(viewModelYayin);
            }
           
        }
    }
}