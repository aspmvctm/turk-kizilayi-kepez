﻿using System.Web.Mvc;

namespace TurkKizilayi.Web.Controllers
{
    public class YonetimController : Controller
    {
        // GET: Yonetim
        public ActionResult Baskan()
        {
            return View();
        }

        public ActionResult YonetimKurulu()
        {
            return View();
        }

        public ActionResult SubeBilgisi()
        {
            return View();
        }
    }
}