﻿using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using Altyapi.Core.Infrastructure;
using Altyapi.Core.Repository;

namespace TurkKizilayi.Web.Class
{
    public class BootStrapper
    {
        public static void RunConfig() {

            BuildAutofac();
        }

        private static void BuildAutofac() {

            var builder = new ContainerBuilder();

            builder.RegisterType<EtiketRepository>().As<IEtiketRepository>();
            builder.RegisterType<KategoriRepository>().As<IKategoriRepository>();
            builder.RegisterType<YayinRepository>().As<IYayinRepository>();
            builder.RegisterType<KullaniciRepository>().As<IKullaniciRepository>();
            builder.RegisterType<ResimRepository>().As<IResimRepository>();
            builder.RegisterType<RolRepository>().As<IRolRepository>();
            builder.RegisterType<YayinRepository>().As<IYayinRepository>();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}